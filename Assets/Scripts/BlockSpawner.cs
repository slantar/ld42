﻿using System.Collections;
using UnityEngine;

public class BlockSpawner : MonoBehaviour
{

    [SerializeField] private bool drawGizmos = true;
    [SerializeField] private Vector2 blockSize;
    [SerializeField] private Vector2Int spawnerSize;
    [SerializeField] private GameObject[] blocksPrefab;
    [SerializeField] private GameObject[] powerUps;
    [SerializeField] private float timeBetweenSpawn = 1f;
    [SerializeField, Range(0,1)] private float powerUpOcurrence = .1f;
    [SerializeField] private int maxTimesToTryInstantiate = 5;

    private WaitForSeconds waitForSeconds;
    private Block[,] blocks;

    private void Start()
    {
        waitForSeconds = new WaitForSeconds(timeBetweenSpawn);
        blocks = new Block[spawnerSize.x, spawnerSize.y];
    }

    public void StartSpawning()
    {
        StartCoroutine(SpawningCoroutine());
    }

    public void StopSpawning()
    {
        StopAllCoroutines();
    }

    private IEnumerator SpawningCoroutine()
    {
        yield return waitForSeconds;
        while (enabled)
        {
            Vector2Int validPosition = GetValidPositionInGrid();
            if(validPosition.x != -1)
            {
                Block block = Instantiate(blocksPrefab[Random.Range(0, blocksPrefab.Length)],
                    GetPosition(validPosition.x, validPosition.y),
                    Quaternion.identity, transform).GetComponent<Block>();
                if(Random.value < powerUpOcurrence)
                {
                    block.PowerUp = powerUps[Random.Range(0, powerUps.Length)];
                }
                blocks[validPosition.x, validPosition.y] = block;
            }
            yield return waitForSeconds;
        }
    }

    private void OnDrawGizmos()
    {
        if (drawGizmos)
        {
            for (int i = 0; i < spawnerSize.x; i++)
            {
                for (int j = 0; j < spawnerSize.y; j++)
                {
                    Gizmos.color = Color.white;
                    Gizmos.DrawWireCube(GetPosition(i, j), blockSize);
                }
            }
        }
    }

    private Vector3 GetPosition(int i, int j)
    {
        return transform.position + new Vector3(i * blockSize.x + blockSize.x * 0.5f, -(j * blockSize.y + blockSize.y * 0.5f), 0);
    }

    private Vector2Int GetValidPositionInGrid()
    {

        int i;
        int j;
        int times = 0;
        do
        {
            i = Random.Range(0, spawnerSize.x);
            j = Random.Range(0, spawnerSize.y);
            times++;
        } while (blocks[i, j] != null && times <= maxTimesToTryInstantiate);

        return blocks[i, j] == null ? new Vector2Int(i, j) : new Vector2Int(-1, 0);
    }
}