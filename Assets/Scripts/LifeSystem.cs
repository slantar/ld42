﻿using System.Collections.Generic;
using UnityEngine;

public class LifeSystem : MonoBehaviour {

    private List<GameObject> lifes;
    private int currentLife;

    private void Awake()
    {
        lifes = new List<GameObject>();
        foreach(Transform t in transform)
        {
            lifes.Add(t.gameObject);
        }
    }

    // Use this for initialization
    void Start () {
        currentLife = lifes.Count - 1;
	}
	
    //Returns true if lifes are empty;
	public bool DecreaseLife()
    {
        lifes[currentLife].SetActive(false);
        currentLife--;
        return currentLife < 0;
    }
}
