﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazzardZone : MonoBehaviour {

    [SerializeField] private Zones zone;
    [SerializeField] private GameObject explosionParticles;

    private void OnTriggerEnter2D(Collider2D collider)
    {
        Ball ball = collider.GetComponent<Ball>();
        if(ball != null)
        {
            Instantiate(explosionParticles, ball.Position, Quaternion.identity);
            GameplayDirector.LifeMissed(zone);
            GameplayDirector.DoScreenShake();
            GetComponent<AudioSource>().Play();
            Destroy(ball.gameObject);
        }
    }
}

public enum Zones
{
    Upper, Lower
}
