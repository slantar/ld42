﻿using System.Collections;
using UnityEngine;
using DG.Tweening;

public class Ball : MonoBehaviour {

    [SerializeField] private float speed = 500;
    [SerializeField] private float minSpeed = 200;
    [SerializeField] private float maxSpeed = 1500;
    [SerializeField] private Vector2 direction;
    [SerializeField] private LayerMask bounceWith;
    [SerializeField] private ParticleSystem respawn;
    [SerializeField] private ParticleSystem speedIncrease;
    [SerializeField] private ParticleSystem speedDecrease;
    [SerializeField] private AudioSource bounceWall;
    [SerializeField] private AudioSource respawnSound;
    [SerializeField] private AudioSource bounceBlock;
    [SerializeField] private AudioSource bigBallIncrease;
    [SerializeField] private float pitchIncrement = 0.1f;


    private new Rigidbody2D rigidbody;
    private BoxCollider2D boxCollider;
    Vector2 velocity;
    private int maxFramesStayingCollission = 10;
    private int currentFramesStayingCollission = 0;
    private bool isGiant;
    private int giantCurrentTime;

    public Vector2 Direction {
        get
        {
            return direction.normalized;
        }
        set
        {
            direction = value;
        }
    }

    public float Speed
    {
        get
        {
            return speed;
        }
        set
        {
            speed = Mathf.Clamp(value, minSpeed, maxSpeed);
        }
    }

    public Vector2 Position
    {
        get
        {
            return transform.position;
        }
        set
        {
            transform.position = value;
        }
    }

    public Vector2 LocalPosition
    {
        get
        {
            return transform.localPosition;
        }
        set
        {
            transform.localPosition = value;
        }
    }

    public Transform Parent
    {
        set
        {
            transform.SetParent(value);
        }
    }

    public bool CollissionsEnabled
    {
        get
        {
            return boxCollider.enabled;
        }
        set
        {
            boxCollider.enabled = value;
            if (value)
            {
                rigidbody.bodyType = RigidbodyType2D.Dynamic;
            }
            else
            {
                rigidbody.bodyType = RigidbodyType2D.Kinematic;
            }
        }
    }

    private int scoreFactor;

    public int ScoreFactor {
        get {
            return scoreFactor;
        }
        set
        {
            GameplayDirector.SetCombo(value);
            scoreFactor = value;
        }
    }

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        boxCollider = GetComponent<BoxCollider2D>();
    }

    private void Start()
    {
        scoreFactor = 1;
        respawn.Play();
    }

    // Update is called once per frame
    void Update() {
        if (boxCollider.enabled)
        {
            velocity = Direction * speed * Time.deltaTime;
            rigidbody.velocity = velocity;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(bounceWith != (bounceWith | (1 << collision.collider.gameObject.layer)))
        {
            float weightFactor = 1;
            if (isGiant)
            {
                GameplayDirector.MakeBounceShake(Direction);
                weightFactor = 0.8f;
            }
            if (collision.collider.GetComponent<Block>() == null)
            {
                bounceWall.pitch = weightFactor;
                bounceWall.PlayOneShot(bounceWall.clip);
            }
            else
            {
                bounceBlock.PlayOneShot(bounceBlock.clip);
                bounceBlock.pitch = weightFactor + scoreFactor * pitchIncrement;
            }
            Direction = Vector3.Reflect(Direction, collision.GetContact(0).normal);
        }

    }

    public void PlaceBall(Transform parent)
    {
        Parent = parent;
        LocalPosition = Vector2.zero;
    }

    public void PlayIncreasedSpeedParticles()
    {
        speedIncrease.Play();
        respawn.Play();
        respawnSound.pitch = Random.Range(.9f, 1.3f);
        respawnSound.Play();
    }

    public void PlayDecreaseSpeedParticles()
    {
        speedDecrease.Play();
        respawnSound.pitch = Random.Range(.9f, 1.3f);
        respawn.Play();
    }
    
    public void SetGiantBall()
    {
        boxCollider.size = new Vector2(1, 1);
    }

    public void SetGiantBall(int seconds)
    {
        if (!isGiant)
        {
            StartCoroutine(GiantBallCoroutine(seconds));
        }
        else
        {
            giantCurrentTime = 0;
        }
    }

    private IEnumerator GiantBallCoroutine(int seconds)
    {
        isGiant = true;
        giantCurrentTime = 0;
        boxCollider.size = Vector2.one;
        bigBallIncrease.pitch = 1;
        bigBallIncrease.Play();
        transform.GetChild(0).DOScale(Vector3.one, 0.3f).SetEase(Ease.OutBack);
        while(giantCurrentTime < seconds)
        {
            yield return new WaitForSeconds(1);
            giantCurrentTime++;
        }
        isGiant = false;
        bigBallIncrease.pitch = -1;
        bigBallIncrease.Play();
        transform.GetChild(0).DOScale(new Vector3(0.3f, 0.3f, 0.3f), 0.3f);
        boxCollider.size = new Vector2(0.3f, 0.3f);
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (maxFramesStayingCollission <= currentFramesStayingCollission)
        {
            OnCollisionEnter2D(collision);
            currentFramesStayingCollission = 0;
        }
        else
        {
            currentFramesStayingCollission++;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        currentFramesStayingCollission = 0;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawLine(transform.position, transform.position + (Vector3)(Direction));
    }
}
