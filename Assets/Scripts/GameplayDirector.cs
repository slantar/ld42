﻿
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameplayDirector : MonoBehaviour
{
    private static GameplayDirector instance;

    [SerializeField] private GameObject ballPrefab;
    [SerializeField] private BlockSpawner blockSpawner;
    [SerializeField] private Plate upperPlate;
    [SerializeField] private Plate lowerPlate;
    [SerializeField] private GameOverController gameOverController;
    [SerializeField] private GameCameraController cameraController;
    [SerializeField] private GameObject pauseMenu;
    [Header("ArcadeMode")]
    [SerializeField] private LifeSystem arcadeLifeSystem;
    [SerializeField] private NumberPanel highScorePanel;
    [SerializeField] private NumberPanel scorePanel;
    [SerializeField] private TextMeshProUGUI comboText;

    private Ball currentBall;
    private GameMode gameMode = GameMode.Arcade;
    private bool gameStarted;

    private const string HIGH_SCORE_KEY = "highScore";


    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        highScorePanel.Value = PlayerPrefs.GetInt(HIGH_SCORE_KEY);
    }

    private void Update()
    {
        if(gameStarted && Input.GetKeyDown(KeyCode.P))
        {
            Time.timeScale = Time.timeScale == 0 ? 1 : 0;
            pauseMenu.SetActive(Time.timeScale == 0);
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            RestartGame();
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    public static void DestroyBall()
    {
        GameOver();
    }

    public static void LifeMissed(Zones zone)
    {
        if (instance.arcadeLifeSystem.DecreaseLife())
        {
            GameOver();
        }
        else
        {
            instance.blockSpawner.StopSpawning();
            PutNewBall(zone);
        }
    }

    private static void PutNewBall(Zones zone)
    {
        DOTween.Sequence()
              .AppendCallback(() =>
              {
                  instance.upperPlate.MovementEnabled = false;
                  instance.lowerPlate.MovementEnabled = false;
              }).AppendInterval(1)
              .AppendCallback(() =>
              {
                  instance.upperPlate.MoveToOrigin();
                  instance.lowerPlate.MoveToOrigin();
              }).AppendInterval(0.5f)
              .AppendCallback(() => {
                  instance.upperPlate.MovementEnabled = true;
                  instance.lowerPlate.MovementEnabled = true;
                  PutBallIn(zone);
              });


    }

    private static void PutBallIn(Zones zone)
    {
        instance.currentBall = Instantiate(instance.ballPrefab).GetComponent<Ball>();
        if (zone == Zones.Upper)
        {
            instance.upperPlate.SpawnBall(instance.currentBall);
        }
        else
        {
            instance.lowerPlate.SpawnBall(instance.currentBall);
        }
    }

    private static void GameOver()
    {
        instance.gameOverController.ShowGameOver();
        PlayerPrefs.SetInt(HIGH_SCORE_KEY, instance.highScorePanel.Value);
    }

    public static void StartGame()
    {
        instance.gameStarted = true;
        PutBallIn(Random.value > 0.5f? Zones.Upper : Zones.Lower);
        instance.upperPlate.Spawn();
        instance.lowerPlate.Spawn();
    }

    public static void StartSpawnBlocks()
    {
        instance.blockSpawner.StartSpawning();
    }

    public static void RestartGame()
    {
        SceneManager.LoadScene(0);
    }

    public static void AddScore(int scoreToAdd)
    {
        instance.scorePanel.Value += scoreToAdd;
        if(instance.scorePanel.Value >= instance.highScorePanel.Value)
        {
            instance.highScorePanel.Value = instance.scorePanel.Value;
        }
    }

    public static void AddSpeedToBall(float speed)
    {
        instance.currentBall.Speed += speed;
        if(speed > 0)
        {
            instance.currentBall.PlayIncreasedSpeedParticles();
        }
        else
        {
            instance.currentBall.PlayDecreaseSpeedParticles();
        }
    }

    public static void SetGiantBall(int time)
    {
        instance.currentBall.SetGiantBall(time);
    }

    public static void MakeBounceShake(Vector2 direction)
    {
        instance.cameraController.GiantBallShake(instance.currentBall.Direction);
    }

    public static void SetCombo(int combo)
    {
        instance.comboText.text = (combo - 1).ToString();
    }

    public static void DoScreenShake()
    {
        instance.cameraController.DoScreenShake();
    }
}

public enum GameMode
{
    Arcade,
    TwoPlayers
}
