﻿using DG.Tweening;
using UnityEngine;

public class GameCameraController : MonoBehaviour {

    [SerializeField] private float force = 1;
    [SerializeField] private DOTweenAnimation giantShake;
    [SerializeField] private DOTweenAnimation screenShake;

    public void GiantBallShake(Vector2 direction)
    {
        giantShake.endValueV3 = direction * force;
        giantShake.DORestartById(giantShake.id);
    }

    public void DoScreenShake()
    {
        screenShake.DORestartById(screenShake.id);
    }
}
