﻿using UnityEngine;

public class GiantBallPowerUp : PowerUp
{
    [SerializeField] private int giantTime = 10;

    protected override void ApplyPowerUp()
    {
        GameplayDirector.SetGiantBall(giantTime);
    }
}
