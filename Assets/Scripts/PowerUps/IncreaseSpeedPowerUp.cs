﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreaseSpeedPowerUp : PowerUp
{
    [SerializeField] private float speedToIncrease = 50;
    protected override void ApplyPowerUp()
    {
        GameplayDirector.AddSpeedToBall(speedToIncrease);
    }
}
