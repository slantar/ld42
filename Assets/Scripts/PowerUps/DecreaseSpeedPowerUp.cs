﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecreaseSpeedPowerUp : PowerUp
{
    [SerializeField] private float speedToDecrease = 50;
    protected override void ApplyPowerUp()
    {
        GameplayDirector.AddSpeedToBall(-speedToDecrease);
    }
}
