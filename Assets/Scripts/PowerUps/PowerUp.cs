﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PowerUp : MonoBehaviour {
    [SerializeField] private float speed;
    private Vector3 velocity;

    
	void Start () {
		if(transform.position.y < 0)
        {
            speed = -speed;
        }
	}

    private void Update()
    {
        velocity.y = speed;
        transform.position += velocity * Time.deltaTime;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Plate plate = collision.GetComponent<Plate>();
        if(plate != null)
        {
            ApplyPowerUp();
            Destroy(gameObject);
        }
    }

    protected abstract void ApplyPowerUp();
}
