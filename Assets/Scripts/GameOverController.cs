﻿using TMPro;
using UnityEngine;
using DG.Tweening;

public class GameOverController : MonoBehaviour {

    [SerializeField] private float delayToShowGameOver = 1;
    [SerializeField] private float intervalToMainMenu;
    [SerializeField] private GameObject gameOverText;

    public void ShowGameOver()
    {
        AudioSource audioSorce = GetComponent<AudioSource>();
        DOTween.Sequence().SetUpdate(true)
            .AppendInterval(delayToShowGameOver)
            .AppendCallback(() => {
                gameOverText.SetActive(true);
                audioSorce.Play();
                Time.timeScale = 0;
                })
            .AppendInterval(audioSorce.clip.length)
            .AppendCallback(() => {
                GameplayDirector.RestartGame();
            });
    }
}
