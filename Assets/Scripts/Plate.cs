﻿using UnityEngine;
using DG.Tweening;

public class Plate : MonoBehaviour {

    [SerializeField] private string MovementAxis = "Horizontal";
    [SerializeField] private float speed = 10;
    [SerializeField] private DOTweenAnimation recoilAnimation;
    [SerializeField] private DOTweenAnimation startAnimation;
    [Header("Soot ball parameters")]
    [SerializeField] private KeyCode shootKey = KeyCode.Space;
    [SerializeField] private Transform ballStart;
    [SerializeField] private Transform bouncePivot;
    [SerializeField] private Vector2 shootBallDirection;
    [SerializeField] private float ballStartSpeed;

    private new Rigidbody2D rigidbody;
    private Ball myBall;
    private Vector2 velocity;
    private bool movementEnabled;
    private AudioSource bounceSound;


    public bool MovementEnabled {
        get {
            return movementEnabled;
        }
        set
        {
            rigidbody.velocity = Vector2.zero;
            movementEnabled = value;
        }
    }

    public Vector2 Position
    {
        get
        {
            
            return transform.position;
        }
        set
        {
            transform.position = value;
        }
    }

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        bounceSound = GetComponent<AudioSource>();
    }

    private void Start()
    {
        movementEnabled = true;
    }

    private void Update()
    {
        if(myBall != null && Input.GetKeyDown(shootKey) && Time.timeScale != 0)
        {
            LiberateBall();
            bounceSound.Play();
        }
    }

    private void LiberateBall()
    {
        myBall.CollissionsEnabled = true;
        myBall.Direction = shootBallDirection;
        myBall.Parent = null;
        myBall.Speed = ballStartSpeed;
        recoilAnimation.DORestartById(recoilAnimation.id);
        GameplayDirector.StartSpawnBlocks();
        myBall = null;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Ball ball = collision.collider.GetComponent<Ball>();
        if(ball != null)
        {
            ball.Direction = (collision.GetContact(0).point - (Vector2)bouncePivot.position).normalized;
            recoilAnimation.DORestartById(recoilAnimation.id);
            ball.ScoreFactor = 1;
            bounceSound.pitch = Random.Range(.9f, 1.3f);
            bounceSound.Play();
        }
    }

    private void FixedUpdate()
    {
        if (movementEnabled)
        {
            velocity.x = Input.GetAxisRaw(MovementAxis) * speed * Time.deltaTime;
            rigidbody.velocity = velocity;
        }
    }

    public void Spawn()
    {
        startAnimation.DORestartById(startAnimation.id);
    }

    public void MoveToOrigin()
    {
        MovementEnabled = false;
        transform.DOMoveX(0, 0.5f);

    }

    public void SpawnBall(Ball ball)
    {
        myBall = ball;
        ball.PlaceBall(ballStart);
        ball.CollissionsEnabled = false;
        myBall.Direction = Vector2.zero;
        myBall.Speed = 0;
    }
}
