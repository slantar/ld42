﻿using UnityEngine;
using System.Collections;

public class Chronometer
{
    private float accumulatedTime;
    private float startTime;
    private bool paused;
    private bool unscaledTime;
    
    private float CurrentTime
    {
        get
        {
            return unscaledTime ? Time.unscaledTime : Time.time;
        }
    }

    public float TimeElapsed
    {
        get
        {
            return accumulatedTime + (paused? 0 : (CurrentTime - startTime));
        }
    }

    public Chronometer() : this(false) { }

    public Chronometer(bool unscaledTime)
    {
        this.unscaledTime = unscaledTime;
    }

    public void Start(float addSeconds = 0)
    {
        paused = false;
        accumulatedTime = Mathf.Clamp(addSeconds, 0, float.MaxValue);
        startTime = CurrentTime;
    }

    public void Pause()
    {
        accumulatedTime = TimeElapsed;
        paused = true;
    }

    public void Continue()
    {
        paused = false;
        startTime = CurrentTime;
    }
}
