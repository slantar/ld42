﻿using UnityEngine;
using DG.Tweening;

public class MenuController : MonoBehaviour {

    [SerializeField] private KeyCode startKey;
    [SerializeField] private GameObject startLogo;
    private bool startTriggered;

    private void Awake()
    {
        Time.timeScale = 0;
    }

    private void Update()
    {
        if (!startTriggered && Input.GetKeyDown(startKey))
        {
            startTriggered = true;
            GetComponent<AudioSource>().Play();
            Sequence sequence = DOTween.Sequence().SetUpdate(true);
            for(int i = 0; i < 3; i++)
            {
                sequence.AppendCallback(() => startLogo.SetActive(false));
                sequence.AppendInterval(0.1f);
                sequence.AppendCallback(() => startLogo.SetActive(true));
                sequence.AppendInterval(0.1f);

            }
            sequence.AppendInterval(0.3f);
            sequence.AppendCallback(() => {
                gameObject.SetActive(false);
                Time.timeScale = 1;
                GameplayDirector.StartGame();
            });
        }
    }

}
