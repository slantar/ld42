﻿using DG.Tweening;
using UnityEngine;

public class Block : MonoBehaviour {
    [SerializeField] private float increaseBallSpeed = 5f;

    [SerializeField] private GameObject[] states;
    [SerializeField] private int score = 5;
    [SerializeField] private GameObject hitParticlesPrefab;
    [SerializeField] private ParticleSystem anticipationParticles;
    private int currentBlock = 0;
    private DOTweenAnimation shakeAnimation;
    private float initialPitch;

    public GameObject PowerUp { get; set; }

    private void Awake()
    {
        shakeAnimation = GetComponentInChildren<DOTweenAnimation>();
    }

    private void Start()
    {
        anticipationParticles.Play();
        DOTween.Sequence().InsertCallback(anticipationParticles.main.duration, ShowBlock);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Ball ball = collision.collider.GetComponent<Ball>();
        if(ball != null)
        {
            ball.Speed += increaseBallSpeed;
            states[currentBlock].SetActive(false);
            GameplayDirector.AddScore(score * ball.ScoreFactor);

            InstanceHitParticles();
            currentBlock++;
            if(currentBlock >= states.Length)
            {
                if (PowerUp != null)
                {
                    Destroy(Instantiate(PowerUp, transform.position, Quaternion.identity), 20);
                }
                Destroy(gameObject);
            }
            else
            {
                states[currentBlock].SetActive(true);
                shakeAnimation.DORestart();
            }
            ball.ScoreFactor++;
        }
    }

    private void InstanceHitParticles()
    {
        GameObject go = Instantiate(hitParticlesPrefab, transform.position, Quaternion.identity);
        Destroy(go, 1);
    }

    private void ShowBlock()
    {
        GetComponent<Collider2D>().enabled = true;
        states[currentBlock].SetActive(true);
    }
}
