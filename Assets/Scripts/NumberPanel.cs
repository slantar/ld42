﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NumberPanel : MonoBehaviour {

    [SerializeField] private int zerosToPad;
    [SerializeField] private TextMeshProUGUI numberPanel;

    private string padding;
    private int maxValue;
    private int value;

    public int Value {
        get
        {
            return value;
        }
        set
        {
            numberPanel.text = Mathf.Clamp(value, 0, maxValue).ToString(padding);
            this.value = value;
        }
    }

    private void Awake()
    {
        padding = "D" + zerosToPad;
        maxValue = (int)Mathf.Pow(10, zerosToPad + 1) - 1;
    }


}
